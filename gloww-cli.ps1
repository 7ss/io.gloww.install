#! /usr/bin/pwsh
param (
    [Parameter(ParameterSetName='NewSite', Mandatory=$true)] [string] $sitename,
 [Parameter(ParameterSetName='NewSite', Mandatory=$true)] [string] $siteurl,
    [Parameter(ParameterSetName='NewSite', Mandatory=$true)] [int] $httpport,
    [Parameter(ParameterSetName='NewSite', Mandatory=$true)] [int] $httpsport,
    [Parameter(ParameterSetName='NewSite')] [string] $publiccert,
    [Parameter(ParameterSetName='NewSite')] [string] $privatekey
)
function getAppConfiguration {
    param ([string] $sitename)
"{
 ""Logging"": {
  ""LogLevel"": {
  ""Default"": ""Debug"",
  ""System"": ""Information"",
  ""Microsoft"": ""Information""
  }
 },
 ""Serilog"": {
  ""Using"": [
   ""Serilog.Sinks.Console""
  ],
  ""MinimumLevel"": ""Debug"",
  ""WriteTo"": [
   {
    ""Name"": ""Console""
   },
   {
    ""Name"": ""File"",
    ""Args"": {
    ""path"": ""/var/log/gloww/${sitename}/kestrel.log""
    }
   }
  ],
  ""Enrich"": [
   ""FromLogContext"",
   ""WithMachineName"",
   ""WithThreadId""
  ],
  ""Destructure"": [
   {
    ""Name"": ""ToMaximumDepth"",
    ""Args"": {
    ""maximumDestructuringDepth"": 4
    }
   },
   {
    ""Name"": ""ToMaximumStringLength"",
    ""Args"": {
    ""maximumStringLength"": 100
    }
   },
   {
    ""Name"": ""ToMaximumCollectionCount"",
    ""Args"": {
    ""maximumCollectionCount"": 10
    }
   }
  ],
  ""Properties"": {
  ""Application"": ""Sample""
  }
 },
 ""Swagger"": {
  ""Title"": ""IO.GLOWW.WebEngine DEMO"",
  ""Description"": ""This API allow..."",
  ""Version"": ""v1"",
  ""TermsOfService"": ""http://www.gloww.IO/TOS.html"",
  ""Contact"": {
  ""Name"": ""Charles CHRISTOPH"",
  ""Email"": ""Charles@7ss.be"",
  ""Url"": ""http://www.gloww.io/charles.html""
  },
  ""License"": {
  ""Name"": ""7SS"",
  ""Url"": ""http://www.gloww.io/licence.7ss.html""
  }
 },
 ""JwtSettings"": {
  ""Secret"": ""THIS IS USED TO SIGN AND VERIFY JWT TOKENS, REPLACE IT WITH YOUR OWN SECRET, IT CAN BE ANY STRING""
 },
 ""SPA"": [
  {
  ""webPath"": ""/GlowwMgmt"",
  ""physicalPath"": ""/usr/lib/gloww/qual/GlowwMgmt/distqual""
  }
 ]
}
"
}
function getServiceConfiguration {
    param ([string] $sitename,[int] $httpport,[int] $httpsport)

"
[Unit]
Description=GLOWW instance Service for running ${sitename} appli
[Service]
WorkingDirectory=/etc/gloww/${sitename}
ExecStart=/usr/bin/dotnet /usr/lib/gloww/qual/IO.GLOWW.WebEngine.dll --urls ""https://0.0.0.0:${httpsport};http://0.0.0.0:${httpport}"" --environment ""${sitename}""
Restart=always
# Restart service after 10 seconds if the dotnet service crashes:
RestartSec=10
SyslogIdentifier=GLOWW.${sitename}
User=root
Environment=PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
[Install]
WantedBy=multi-user.target
";
}
function getNginxConfiguration {
    param ([string] $siteurl, [int] $httpport, [int] $httpsport,[string] $publiccert,[string] $privatekey)
"server {
 listen    80;
 server_name    $siteurl;
 location / {
  include proxy_params;
  proxy_pass https://${siteurl}:$httpport;
 }
 location /.well-known/acme-challenge {
  alias /etc/nginx/sslforfree/.well-known/acme-challenge;
 }
}
server {
 listen    443 ssl;
 server_name    ${siteurl};
 ssl_certificate     $publiccert;//conf.d/certificates/gloww_io.cer
 ssl_certificate_key $privatekey;//conf.d/certificates/gloww_io.key;
 ssl_protocols       TLSv1 TLSv1.1 TLSv1.2;
 ssl_ciphers         HIGH:!aNULL:!MD5;
 location / {
  include proxy_params;
  proxy_pass https://${siteurl}:${httpsport};
 }
}
";
}
$path = 'c:\temp\testinstallgloww';
    if ($IsLinux) {
        $path = '';
    }
if (-not (Test-Path "$path/etc/gloww/$sitename")) {New-Item "$path/etc/gloww/$sitename" -ItemType Directory}
if (-not (Test-Path "$path/etc/systemd/system")) {New-Item "$path/etc/systemd/system" -ItemType Directory}
if (-not (Test-Path "$path/etc/nginx/conf.d")) {New-Item "$path/etc/nginx/conf.d" -ItemType Directory }
getAppConfiguration -sitename $sitename | Out-File "$path/etc/gloww/$sitename/appsettings.$sitename.json"
getServiceConfiguration -sitename $sitename -httpport $httpport -httpsport $httpsport | Out-File "$path/etc/systemd/system/GLOWW-${sitename}.service"
getNginxConfiguration -siteurl $siteurl -httpport $httpport -httpsport $httpsport  | Out-File "$path/etc/nginx/conf.d/GLOWW-${sitename}.conf"

